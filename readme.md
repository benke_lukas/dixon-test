# Dixon test

Solution for test given at (http://tech.dixons.cz/test)

## Notes
1. I have assumed that the outside framework contains full-featured IoC container. So swapping of Cache implementation can be done at bootstrap by reading the config and providing correct concrete implementation for the ICache contract.
2. Because both, elastic search and mysql returns same data, I have implemented the ProductsRepository for both drivers (IElasticSearch and IMySQLDriver), again, which driver to use should be decided by some config value at bootstrap.
3. In the test directory are some simple test, without the PHPUnit test framework.
