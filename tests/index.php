<?php
/**
 * Created by PhpStorm.
 * User: ben
 * Date: 10.11.16
 * Time: 17:10
 */
require_once '../vendor/autoload.php';

//test FileCache
$fileCache = new \Acme\Cache\FileCache();

//test cache put
$fileCache->put('product_1', [
    'name' => 'asd',
    'price' => 500
]);
//test cache get
print_r($fileCache->get('product_1'));

//test cache has
var_dump($fileCache->has('product_1'));

//test product request increment
$productRequestIncrementer = new \Acme\Product\Services\ProductsRequestCounter();

$previousValue = $productRequestIncrementer->getCount(1);
$productRequestIncrementer->increment(1);
var_dump(
    ($productRequestIncrementer->getCount(1) > $previousValue)
);