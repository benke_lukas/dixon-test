<?php
/**
 * Created by PhpStorm.
 * User: ben
 * Date: 10.11.16
 * Time: 15:26
 */

namespace Acme\Cache;


use Acme\Cache\Contracts\ICache;
use Acme\Core\Traits\Exceptions\InvalidJsonFileException;
use Acme\Core\Traits\WorksWithJsonFile;

class FileCache implements ICache
{
    use WorksWithJsonFile;

    private $file;

    /**
     * FileCache constructor.
     * @param string $file
     */
    public function __construct($file = '/cache.json')
    {
        $this->file = $file;
        $this->createFileIfItDoesntExists();
    }

    public function put($key, $value)
    {
        try {
            $existing = $this->getFileAsArray();

            $existing[$key] = $value;

            $this->setFileAsJson($existing);

            return true;
        } catch ( InvalidJsonFileException $e ) {
            return false;
        }
    }

    public function get($key, $default = null)
    {
        try {
            $existing = $this->getFileAsArray();

            if (isset($existing[$key])) {
                return $existing[$key];
            }

            return $default;
        } catch ( InvalidJsonFileException $e ) {
            return false;
        }
    }

    public function has($key)
    {
        try {
            $existing = $this->getFileAsArray();

            return isset($existing[$key]);
        } catch ( InvalidJsonFileException $e ) {
            return false;
        }
    }
}