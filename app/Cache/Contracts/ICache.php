<?php
/**
 * Created by PhpStorm.
 * User: ben
 * Date: 10.11.16
 * Time: 15:25
 */

namespace Acme\Cache\Contracts;


interface ICache
{

    /**
     * Puts item in cache
     *
     * @param $key
     * @param $value
     * @return boolean
     */
    public function put($key, $value);

    /**
     * Gets item in cache by key
     *
     * @param $key
     * @param null $default Default return value
     * @return mixed
     */
    public function get($key, $default = null);

    /**
     * Checks if item exists in cache
     *
     * @param $key
     * @return boolean
     */
    public function has($key);
}