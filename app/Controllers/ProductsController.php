<?php
/**
 * Created by PhpStorm.
 * User: ben
 * Date: 10.11.16
 * Time: 15:19
 */

namespace Acme\Controllers;


use Acme\Cache\Contracts\ICache;
use Acme\Product\Repositories\Contracts\IProductsRepository;
use Acme\Product\Services\Contracts\IProductsRequestCounter;

class ProductsController
{

    /**
     * @var ICache
     */
    private $cache;
    /**
     * @var IProductsRepository
     */
    private $productsRepository;
    /**
     * @var IProductsRequestCounter
     */
    private $productsRequestCounter;

    /**
     * ProductsController constructor.
     * @param ICache $cache
     * @param IProductsRepository $productsRepository
     * @param IProductsRequestCounter $productsRequestCounter
     */
    public function __construct(
        ICache $cache,
        IProductsRepository $productsRepository,
        IProductsRequestCounter $productsRequestCounter
    )
    {
        $this->cache = $cache;
        $this->productsRepository = $productsRepository;
        $this->productsRequestCounter = $productsRequestCounter;
    }

    /**
     * @param $id
     * @return mixed
     */
    public function detail($id) {
        $cache_key = "product_{$id}";
        //if cached, retrieve from cache
        $cached = $this->cache->get($cache_key, false);
        if ($cached !== false) {
            $this->productsRequestCounter->increment($id);
            return $cached;
        }
        //retrieve from MySQL or ElasticSearch
        $product = $this->productsRepository->findById($id);
        $this->cache->put($cache_key, $product);
        //increment the number of requests per product
        $this->productsRequestCounter->increment($id);
        //return product data in JSON
        return json_encode($product);
    }
}