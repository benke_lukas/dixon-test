<?php
/**
 * Created by PhpStorm.
 * User: ben
 * Date: 10.11.16
 * Time: 16:24
 */

namespace Acme\Core\Traits;


use Acme\Core\Traits\Exceptions\InvalidJsonFileException;

trait WorksWithJsonFile
{

    protected function getFileAsArray()
    {
        $content = file_get_contents($this->getFilePath());
        if ( $content == '' ) {
            $decoded = [];
        } else {
            $decoded = json_decode($content, true);
        }

        if (is_array($decoded)) {
            return $decoded;
        }
        throw new InvalidJsonFileException;
    }

    protected function setFileAsJson(array $content)
    {
        $encoded = json_encode($content);

        if ($encoded) {
            file_put_contents($this->getFilePath(), $encoded);
        }

        return false;
    }

    protected function createFileIfItDoesntExists()
    {
        if (!file_exists($this->getFilePath())) {
            touch($this->getFilePath());
        }
    }

    private function getFilePath() {
        return realpath(__DIR__.'/../../') . $this->file;
    }
}