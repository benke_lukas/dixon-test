<?php
/**
 * Created by PhpStorm.
 * User: ben
 * Date: 10.11.16
 * Time: 16:00
 */

namespace Acme\Product\Repositories\Contracts;


interface IProductsRepository
{
    /**
     * Finds product by id
     *
     * @param $id
     * @return mixed
     */
    public function findById($id);
}