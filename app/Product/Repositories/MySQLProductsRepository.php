<?php
/**
 * Created by PhpStorm.
 * User: ben
 * Date: 10.11.16
 * Time: 16:14
 */

namespace Acme\Product\Repositories;

use Acme\Core\Drivers\Contracts\IMySQLDriver;
use Acme\Product\Repositories\Contracts\IProductsRepository;

class MySQLProductsRepository implements IProductsRepository
{

    /**
     * @var IMySQLDriver
     */
    private $driver;

    /**
     * MySQLProductsRepository constructor.
     * @param IMySQLDriver $driver
     */
    public function __construct(
        IMySQLDriver $driver
    )
    {
        $this->driver = $driver;
    }

    public function findById($id)
    {
        return $this->driver->findProduct($id);
    }


}