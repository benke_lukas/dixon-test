<?php
/**
 * Created by PhpStorm.
 * User: ben
 * Date: 10.11.16
 * Time: 15:57
 */

namespace Acme\Product\Repositories;

use Acme\Core\Drivers\Contracts\IElasticSearchDriver;
use Acme\Core\Drivers\Contracts\IMySQLDriver;
use Acme\Product\Repositories\Contracts\IProductsRepository;

class ElasticSearchProductsRepository implements IProductsRepository
{

    /**
     * @var IElasticSearchDriver
     */
    private $driver;


    /**
     * ElasticSearchProductsRepository constructor.
     * @param IElasticSearchDriver $driver
     */
    public function __construct(
        IElasticSearchDriver $driver
    )
    {
        $this->driver = $driver;
    }

    public function findById($id)
    {
        return $this->driver->findById($id);
    }

}