<?php
/**
 * Created by PhpStorm.
 * User: ben
 * Date: 10.11.16
 * Time: 16:19
 */

namespace Acme\Product\Services\Contracts;


interface IProductsRequestCounter
{

    /**
     * Increments request count for product
     *
     * @param $product_id
     * @return mixed
     */
    public function increment($product_id);

    /**
     * Gets count of request for product
     *
     * @param $product_id
     * @return int
     */
    public function getCount($product_id);
}