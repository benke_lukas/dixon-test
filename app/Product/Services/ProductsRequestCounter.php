<?php
/**
 * Created by PhpStorm.
 * User: ben
 * Date: 10.11.16
 * Time: 16:19
 */

namespace Acme\Product\Services;


use Acme\Core\Traits\WorksWithJsonFile;
use Acme\Product\Services\Contracts\IProductsRequestCounter;

class ProductsRequestCounter implements IProductsRequestCounter
{
    use WorksWithJsonFile;
    private $file;

    /**
     * ProductsRequestCounter constructor.
     */
    public function __construct()
    {
        $this->file = '/products_requests.json';
        $this->createFileIfItDoesntExists();
    }


    public function increment($product_id)
    {
        $existing = $this->getFileAsArray();

        if ( isset($existing[$product_id]) ) {
            $value = $existing[$product_id];
            if ( !is_int($value) ) {
                throw new \InvalidArgumentException("Value to increment is not integer");
            }
            $existing[$product_id] += 1;
        } else {
            $existing[$product_id] = 1;
        }
        $this->setFileAsJson($existing);
    }

    public function getCount($product_id)
    {
        $existing = $this->getFileAsArray();

        if ( isset($existing[$product_id]) ) {
            return $existing[$product_id];
        } else {
            return 0;
        }
    }


}